#ifndef _INTERNAL_H_
#define _INTERNAL_H_

#include "studfs.h"

char **split(const char *path);
struct node *find_node_by_name (const char *path);
struct node *find_parent_node(const char *path);
void fs_read_file(unsigned char *buffer, unsigned char *sectors);
struct file_info *get_file_info_by_node(struct node *f_node);
int get_new_node_offset();
int write_node_info_to_parent_sector(char *name, int s_number, int n_number);
int fs_get_free_sector();
int fs_sectors_count(int size);
int fs_grow_file(struct file_info* fi, int new_size);
int fs_count_free_blocks();

#endif /* _INTERNAL_H_ */
