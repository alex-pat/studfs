CC = gcc
CFLAGS = -g -Wall -Wpedantic -Werror -DFUSE_USE_VERSION=29 `pkg-config fuse --cflags`
LINKFLAGS = -Wall `pkg-config fuse --libs`

all: bin/studfs

.PHONY: run test

run: bin/studfs
	bin/studfs -d disk mnt

clean:
	rm -rf bin obj

bin/studfs: obj/studfs.o obj/device.o obj/main.o
	gcc -g -o bin/studfs obj/* $(LINKFLAGS)

obj/main.o: main.c studfs.h device.h dev_layout.h
	gcc $(CFLAGS) -c main.c -o $@

obj/studfs.o: studfs.c studfs.h device.h dev_layout.h
	gcc $(CFLAGS) -c studfs.c -o $@

obj/device.o: device.c device.h
	gcc $(CFLAGS) -c device.c -o $@

test:
	make -f test-make run_tests
