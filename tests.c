#include "kselftest_harness.h"
#include "internal.h"

TEST(_split_)
{
	char *paths[] = {
		"/sdjlkf/asdf/asd/fas/df/a/sdf/sd/f",
		"/asdf/",
		"/",
		"/asdklfj/asdfsad/"
	};
	static const struct {
		const int entries_num;
		const char *entries[20];
	} expected[] = {
		{ 10, { "/", "sdjlkf", "asdf", "asd", "fas", "df", "a", "sdf", "sd", "f" } },
		{ 2, { "/", "asdf" } },
		{ 1, { "/" } },
		{ 3, { "/", "asdklfj", "asdfsad" } }
	};
	for (int i = 0; i < sizeof paths / sizeof paths[0] ; i++ ) {
		char **result = split(paths[i]);
		int j;
		for (j = 0; j < expected[i].entries_num; j++) {
			ASSERT_NE(result[j], NULL);
			ASSERT_FALSE(strcmp(result[j], expected[i].entries[j]));
			free(result[j]);
		}
		ASSERT_EQ(result[j], NULL);
		free(result);
	}
	ASSERT_EQ(split(""), NULL);
}

FIXTURE(def_fixture) {
	FILE *device;
};

FIXTURE_SETUP(def_fixture)
{
	system("dd if=/dev/zero of=testdisk bs=1M count=50 2>/dev/null");
	self->device = device_open("testdisk");
	if (!self->device)
		exit(1);
	fs_init(NULL);
}

FIXTURE_TEARDOWN(def_fixture)
{
	device_close();
	self->device = NULL;
	system("rm testdisk");
}

TEST_F(def_fixture, _mknod_unlink_)
{
	for (int i = 0; i < 200; i++) {
		ASSERT_EQ(fs_mknod("/file/", S_IFREG, 0), 0);
		ASSERT_EQ(fs_unlink("/file"), 0);
	}
}

TEST_F(def_fixture, _mkdir_rmdir_)
{
	for (int i = 0; i < 200; i++) {
		ASSERT_EQ(fs_mkdir("/dir", 0777), 0);
		ASSERT_EQ(fs_rmdir("/dir"), 0);
	}
}

TEST_F(def_fixture, _open_read_change_)
{
	struct fuse_file_info fi;
	char firstbuf[13];
	char secondbuf[16];
	char thirdbuf[29];

	ASSERT_EQ(fs_mknod("/file", S_IFREG, 0), 0);
	ASSERT_EQ(fs_open("/file", &fi), 0);
	ASSERT_EQ(fs_write("/file", "Hello, test!\n",
			13, 0, &fi), 13);
	ASSERT_EQ(fs_read("/file", firstbuf,
			13, 0, &fi), 13);
	ASSERT_EQ(memcmp(firstbuf, "Hello, test!\n", 13), 0);
	ASSERT_EQ(fs_flush("/file", &fi), 0);
	ASSERT_EQ(fs_release("/file", &fi), 0);

	ASSERT_EQ(fs_open("/file", &fi), 0);
	ASSERT_EQ(fs_write("/file", "The second line\n",
			16, 13, &fi), 16);
	ASSERT_EQ(fs_read("/file", secondbuf,
			16, 13, &fi), 16);
	ASSERT_EQ(memcmp(secondbuf, "The second line\n", 16), 0);

	ASSERT_EQ(fs_read("/file", thirdbuf,
			29, 0, &fi), 29);
	ASSERT_EQ(memcmp(thirdbuf, "Hello, test!\nThe second line\n", 29), 0);
	ASSERT_EQ(fs_flush("/file", &fi), 0);
	ASSERT_EQ(fs_release("/file", &fi), 0);

	ASSERT_EQ(fs_unlink("/file"), 0);
}

TEST_HARNESS_MAIN
