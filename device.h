#ifndef _DEVICE_H_
#define _DEVICE_H_

#include <stdio.h>

#define SECTOR_SIZE	512

FILE *device_open(const char *path);
void device_close();
int device_read_sector(unsigned char buffer[], int sector);
int device_write_sector(unsigned char buffer[], int sector);
void device_flush();
void device_clear_sector(int sector, int offset);
unsigned char *device_mmap_sector(int sector);

#endif
