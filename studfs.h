#ifndef _STUDFS_H_
#define _STUDFS_H_

#include <fuse.h>
#include <stdint.h>
#include "dev_layout.h"

struct node {
	char name[MAX_NAME_LEN + 1];
	uint8_t inode_num;
	int16_t size;
	unsigned char sectors[MAX_SECTORS_PER_FILE];
};

struct file_info {
	struct node *f_node;
	unsigned char f_data[MAX_FILE_SIZE];
	int need_update;
};

int fs_getattr(const char *path, struct stat *statbuf);
int fs_mknod(const char *path, mode_t mode, dev_t dev);
int fs_mkdir(const char *path, mode_t mode);
int fs_unlink(const char *path);
int fs_rmdir(const char *path);
int fs_truncate(const char *path, off_t newSize);
int fs_open(const char *path, struct fuse_file_info *fileInfo);
int fs_release(const char *path, struct fuse_file_info *fileInfo);
int fs_read(const char *path, char *buf, size_t size, off_t offset,
		struct fuse_file_info *fileInfo);
int fs_write(const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fileInfo);
int fs_statfs(const char *path, struct statvfs *statInfo);
int fs_flush(const char *path, struct fuse_file_info *fileInfo);
int fs_opendir(const char *path, struct fuse_file_info *fileInfo);
int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		off_t offset, struct fuse_file_info *fileInfo);
void *fs_init(struct fuse_conn_info *conn);
int fs_utimens(const char *, const struct timespec tv[2]);

#endif
