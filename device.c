#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include "device.h"

static FILE *file;

FILE *device_open(const char *path)
{
	file = fopen(path, "r+");
	if (!file)
		perror("device_open: fopen");
	return file;
}

void device_close()
{
	if (fflush(file)) {
		perror("device_close: fflush");
		exit(1);
	}
	if (fclose(file)) {
		perror("device_close: fclose");
		exit(1);
	}
}

int device_read_sector(unsigned char buffer[], int sector)
{
	if (fseek(file, sector * SECTOR_SIZE, SEEK_SET)) {
		perror("device_read_sector: fseek");
		exit(1);
	}

	return fread(buffer, 1, SECTOR_SIZE, file) == SECTOR_SIZE;
	/* TODO: fast reading */
}

int device_write_sector(unsigned char buffer[], int sector)
{
	if (fseek(file, sector * SECTOR_SIZE, SEEK_SET)) {
		perror("device_write_sector: fseek");
		exit(1);
	}
	return fwrite(buffer, 1, SECTOR_SIZE, file) == SECTOR_SIZE;
}

static char zero_sector[SECTOR_SIZE];

void device_clear_sector(int sector, int offset)
{
	int clr_size = SECTOR_SIZE - offset;

	if (fseek(file, sector * SECTOR_SIZE + offset, SEEK_SET)) {
		perror("device_clear_sector: fseek");
		exit(1);
	}
	if (fwrite(zero_sector, 1, clr_size, file) != clr_size) {
		perror("device_clear_sector: fwrite");
		exit(1);
	}
}

void device_flush()
{
	if (fflush(file)) {
		perror("device_flush: fseek");
		exit(1);
	}
}

unsigned char *device_mmap_sector(int sector)
{
	size_t sys_page_size = sysconf(_SC_PAGESIZE);
	unsigned char *res;
	int needed_offset = (sector * SECTOR_SIZE);
	int real_offset = needed_offset & ~(sys_page_size - 1);
	if (sys_page_size % SECTOR_SIZE) {
		puts("Assertion: bad sector size");
		exit(1);
	}
	res = mmap(NULL, SECTOR_SIZE, PROT_READ | PROT_WRITE,
		MAP_SHARED, fileno(file), real_offset);
	if (res == MAP_FAILED) {
		perror("device_mmap_sector: mmap");
		exit(1);
	}
	return res + (needed_offset - real_offset);
}
