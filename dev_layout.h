#ifndef _DEV_LAYOUT_H_
#define _DEV_LAYOUT_H_

/*
 * Studfs layout
 *
 * Device divided by sectors (currently 512 bytes).
 *
 * Sector #0 -- empty. Temporary.
 *
 * Sector #1 -- sectors bitmap.
 *
 * Sector #2 -- nodes.
 *	Node - currently 32 bytes:
 *	============
 *	0	1	2	3	4	5	6	7
 *	Name (6 bytes)					File size (2 bytes)
 *	Allocated sectors numbers (24 bytes)
 *	. . .
 *	. . .
 *	============
 *	Note: two-byte file size is signed, -2 means that node is directory.
 *
 * Sector #3 and beyond -- data sectors.
 *	For regular files - it's just raw file data.
 *	For directory, sector contains dentries.
 *	Dentry - currently 8 bytes:
 *	0	1	2	3	4	5	6	7
 *	Name (6 bytes)					<empty>	node_number
 */

#include <stdint.h>
#include "device.h"

#define MAX_NAME_LEN		6
#define NODE_SIZE		32
#define MAX_SECTORS_PER_FILE	(NODE_SIZE - MAX_NAME_LEN - 2)
#define DENTRY_SIZE		8
#define MAX_FILES_IN_DIR	(SECTOR_SIZE / DENTRY_SIZE)
#define MAX_FILE_SIZE		(MAX_SECTORS_PER_FILE * SECTOR_SIZE)
#define MAX_NODES		(SECTOR_SIZE / NODE_SIZE)
#define FIRST_DATA_SECTOR	3
#define MAP_BIT_SIZE		(SECTOR_SIZE * 8)

struct dev_node {
	uint8_t name[MAX_NAME_LEN];
	int16_t size;
	uint8_t sectors[MAX_SECTORS_PER_FILE];
};
_Static_assert (sizeof(struct dev_node) == NODE_SIZE,
		"struct dev_node size");

struct dev_dentry {
	uint8_t name[MAX_NAME_LEN];
	uint8_t __spare;
	uint8_t node_num;
};
_Static_assert (sizeof(struct dev_dentry) == DENTRY_SIZE,
		"struct dev_dentry size");

struct dev_dir_sector {
	struct dev_dentry dentries[MAX_FILES_IN_DIR];
};
_Static_assert (sizeof(struct dev_dir_sector) == SECTOR_SIZE,
		"struct dev_dir_sector size");

struct dev_nodes_sector {
	struct dev_node nodes[MAX_NODES];
};
_Static_assert (sizeof(struct dev_nodes_sector) == SECTOR_SIZE,
		"struct dev_nodes_sector size");

#endif /* _DEV_LAYOUT_H_ */
