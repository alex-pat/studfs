#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "studfs.h"
#include "device.h"

static unsigned char *map;	// [SECTOR_SIZE];
static struct dev_nodes_sector *nodes;

char **split(const char *path)
{
	char **res = NULL;
	int d_count = 1;
	int i = 0;

	if (strlen(path) <= 1)
		if (path[0] != '/')
			return NULL;

	while (path[++i] != '\0')
		if (path[i-1] == '/' && path[i] != '/')
			d_count++;
	res = malloc(sizeof(char *) * (d_count + 1));
	res[d_count] = NULL;
	res[0] = strndup(path, 1);
	i = 0;
	d_count = 0;

	while (path[i] != '\0') {
		while (path[i] == '/')
			i++;
		const char *str = path + i;
		size_t len = 0;

		if (!*str)
			break;
		while (path[i] != '\0' && path[i] != '/')
			i++, len++;

		res[++d_count] = strndup(str, len);
	}

	return res;
}

int get_node_num_by_name(const char *path)
{
	printf("%s(%s)\n", __func__, path);
	char f_name[7];
	struct dev_dir_sector dir_sector;
	int i, path_index = 1, f_sector_index = 3;
	char **names = split(path);

	if (!names || names[1] == NULL)
		return -1;

	do {
		device_read_sector((void *)&dir_sector, f_sector_index);

		printf("\treaded sector #%d\n", f_sector_index);
		for (i = 0; i < MAX_FILES_IN_DIR
				&& dir_sector.dentries[i].name[0] != '\0';
				i++) {
			strncpy(f_name, (void *)dir_sector.dentries[i].name, MAX_NAME_LEN);
			printf("\tcmp: %s %s\n",
				f_name, names[path_index]);
			if (strncmp(f_name, names[path_index], MAX_NAME_LEN) == 0)
				break;
		}
		if (i == MAX_FILES_IN_DIR || dir_sector.dentries[i].name[0] == '\0') {
			puts("\tnode not found");
			return -1;
		}

		if (nodes->nodes[dir_sector.dentries[i].node_num].size != -2
					&& names[path_index+1] != NULL) {
			puts("\tfile on the spot of the dir");
			return -1;
		}
		f_sector_index = nodes->nodes[dir_sector.dentries[i].node_num].sectors[0];
		path_index++;
	} while (names[path_index]);
	puts("\tsearch ended");
	for (int j = 0; names[j] != NULL; j++)
		free(names[j]);
	free(names);
	return dir_sector.dentries[i].node_num;
}

struct node* find_node_by_name(const char *pth)
{
	printf("%s(%s)\n", __func__, pth);
	struct node* res_node;

	if (strcmp(pth, "/") == 0) {
		res_node = malloc(sizeof(struct node));
		res_node->name[0] = '/';
		res_node->name[1] = '\0';
		res_node->size = -2;
		res_node->inode_num = MAX_NODES;
		res_node->sectors[0] = 3;
		res_node->sectors[1] = 0;
		printf("\tnode: %s, size: %d\n", res_node->name, res_node->size);
		return res_node;
	}

	int node_index = get_node_num_by_name(pth);
	printf("\tnode_index = %d\n", node_index);
	res_node = malloc(sizeof(struct node));
	if (node_index < 0) {
		res_node->size = -1;
	} else {
		res_node->inode_num = node_index;
		strncpy(res_node->name,
			(char *) nodes->nodes[node_index].name,
			MAX_NAME_LEN);
		res_node->size = nodes->nodes[node_index].size;
		memcpy(res_node->sectors,
			nodes->nodes[node_index].sectors,
			MAX_SECTORS_PER_FILE);
	}
	printf("\tnode: %s, size: %d\n", res_node->name, res_node->size);
	return res_node;
}

void delete_node_by_name(const char *path)
{
	printf("%s(%s)\n", __func__, path);

	int node_index = get_node_num_by_name(path);
	if (node_index >= 0)
		memset(nodes->nodes + node_index, 0, sizeof(struct dev_node));
}

int split_parent(char *path)
{
	int i;
	for (i = strlen(path) -1; i >= 0 && path[i] == '/'; i--)
		path[i] = '\0';
	while(i >= 0 && path[i] != '/')
		i--;
	if (i < 0) {
		fputs("BUG: find_parent_node: i < 0", stderr);
		exit(1);
	}
	path[i] = '\0';
	for (int j = i-1; j > 0 && path[j] == '/'; j--)
		path[j] = '\0';
	return i;
}

struct node *find_parent_node(const char* pth)
{
	printf("%s(%s)\n", __func__, pth);

	char *path = strdup(pth);
	int slash;
	struct node *parent;

	slash = split_parent(path);
	parent = find_node_by_name(slash > 0 ? path : "/");
	free(path);
	return parent;
}

void fs_read_file(unsigned char* buffer, unsigned char* sectors)
{
	printf("%s(...)\n", __func__);

	for (int i = 0; sectors[i] != 0; ++i) {
		device_read_sector(buffer, sectors[i]);
		buffer += SECTOR_SIZE;
	}
}

struct file_info* get_file_info_by_node(struct node* f_node)
{
	printf("%s(node with name=%s)\n", __func__, f_node->name);
	struct file_info* result = calloc(1, sizeof(struct file_info));
	result->f_node = f_node;
	result->need_update = 0;
	fs_read_file(result->f_data, f_node->sectors);
	return result;
}

int get_new_node_index()
{
	printf("%s()\n", __func__);

	for (int i = 0; i < MAX_NODES; i++)
		if (nodes->nodes[i].name[0] == '\0')
			return i;
	return -1;
}

int write_node_info_to_parent_sector(char* name, int s_number, int n_number)
{
	printf("%s(name=\"%s\",s_number=%d,n_number=%d)\n", __func__, name, s_number, n_number);
	unsigned char parent_sector[SECTOR_SIZE];
	device_read_sector(parent_sector, s_number);
	int i;
	for (i = 0; i < SECTOR_SIZE; i += DENTRY_SIZE) {
		if (parent_sector[i] == 0) {
			size_t name_len = strlen(name) + 1;

			memcpy(parent_sector + i,
				name,
				name_len > MAX_NAME_LEN
					? MAX_NAME_LEN
					: name_len);
			parent_sector[i + MAX_NAME_LEN] = '\0';
			parent_sector[i + MAX_NAME_LEN + 1] = (uint8_t) n_number;
			device_write_sector(parent_sector, s_number);
			device_flush();
			return 0;
		}
	}
	return -1;
}

static inline void map_set_bit(int sector)
{
	map[sector / 8] |= 1 << (sector % 8);
}

static inline void map_clear_bit(int sector)
{
	map[sector / 8] &= ~(1 << (sector % 8));
}

static inline int map_get_bit(int sector)
{
	return (map[sector / 8] & (1 << (sector % 8))) != 0;
}

int fs_get_free_sector()
{
	int i = FIRST_DATA_SECTOR + 1;	// first data sector root dir
	while (map_get_bit(i) == 1 && i < MAP_BIT_SIZE)	// TODO: optimize
		i++;
	i = i < MAP_BIT_SIZE ? i : -1;
	printf("%s() = %d\n", __func__, i);
	return i;
}

static inline int fs_sectors_count(int size)
{
	return (size + SECTOR_SIZE - 1) / SECTOR_SIZE;
}

int fs_grow_file(struct file_info* fi, int new_size)
{
	printf("%s(fileinfo->node->name=%s, new_size=%d)\n", __func__, fi->f_node->name, new_size);
	struct node* node = fi->f_node;
	int new_sectors_count = fs_sectors_count(new_size);
	int old_sectors_count = fs_sectors_count(node->size);
	int clear_offset = new_size % SECTOR_SIZE;
	if (new_sectors_count > MAX_SECTORS_PER_FILE)
		return -ENOSPC;

	while (old_sectors_count < new_sectors_count) {
		int sector = fs_get_free_sector();
		if (sector == -1)
			return -ENOSPC;
		map_set_bit(sector);
		node->sectors[old_sectors_count] = sector;
		device_clear_sector(sector, clear_offset);
		clear_offset = 0;
		old_sectors_count++;
		printf("\tAllocating sector %d\n", sector);
	}

	int ino = node->inode_num;
	node->size = new_size;

	memcpy(nodes->nodes[ino].sectors, node->sectors, new_sectors_count);
	nodes->nodes[ino].size = node->size;

	return 0;
}

int fs_count_free_blocks()
{
	printf("%s()\n", __func__);
	int i, free_block_count;

	free_block_count = 0;
	for (i = 0; i < SECTOR_SIZE; i++)  {
		if (map_get_bit(i) == 0)
			free_block_count++;
	}

	return free_block_count;
}

////////////////////////////
//    fuse operations     //
////////////////////////////

void *fs_init(struct fuse_conn_info *conn)
{
	printf("\n%s()\n", __func__);
	map = device_mmap_sector(1);
	nodes = (struct dev_nodes_sector *) device_mmap_sector(2);
	return NULL;
}

int fs_getattr(const char *path, struct stat *statbuf)
{
	printf("\n%s(%s)\n", __func__, path);

	int res, path_len = strlen(path);

	statbuf->st_uid = 0;
	statbuf->st_gid = 0;
	if (path_len == 1 && path[0] == '/') {
		statbuf->st_mode = S_IFDIR | S_IRWXU | S_IRWXG | S_IRWXO;
		statbuf->st_nlink = 1;
		statbuf->st_ino = MAX_NODES;
		statbuf->st_size = SECTOR_SIZE;
		statbuf->st_blksize = SECTOR_SIZE;
		statbuf->st_blocks = 1;
		return 0;
	}

	struct node* n = find_node_by_name(path);
	puts("\tOK");
	statbuf->st_blksize = SECTOR_SIZE;
	if (n->size == -1) {
		puts("\tfile not found");
		res = -ENOENT;
	} else {
		statbuf->st_ino = n->inode_num;
		if (n->size == -2) { // if dir
			statbuf->st_mode = S_IFDIR | 0777;
			statbuf->st_nlink = 3;
			res = 0;
		} else {
			statbuf->st_mode = S_IFREG | 0666;
			statbuf->st_nlink = 1;
			statbuf->st_size = n->size;
			res = 0;
		}
	}
	free(n);
	return res;
}

int fs_mknod(const char *pth, mode_t mode, dev_t dev)
{
	struct node *parent;
	char *path;
	char *node_name;
	int node_index;
	size_t name_len;

	printf("\n%s(%s, mode=%u, dev=%lu)\n", __func__, pth, mode, dev);

	if (S_ISREG(mode) == 0)
		return -EPERM;

	parent = find_parent_node(pth);
	if (parent->size != -2) {
		free(parent);
		return -ENOENT;
	}

	path = strdup(pth);
	node_name = path + split_parent(path) + 1;

	node_index = get_new_node_index();
	if (node_index == -1) {
		free(path);
		free(parent);
		return -ENOSPC;
	}
	name_len = strlen(node_name) + 1;

	memcpy(nodes->nodes[node_index].name,
		node_name,
		name_len > MAX_NAME_LEN
			? MAX_NAME_LEN
			: name_len);
	memset(nodes->nodes[node_index].sectors, 0, MAX_SECTORS_PER_FILE);


	if ( -1 == write_node_info_to_parent_sector
			(node_name, parent->sectors[0], node_index)) {
		free(parent);
		free(path);
		return -EFAULT;
	}
	free(parent);
	free(path);
	return 0;
}

int fs_mkdir(const char *pth, mode_t mode)
{
	printf("\n%s(%s)\n", __func__, pth);

	struct node* parent = find_parent_node(pth);
	char *dir_name, *path;

	if (parent->size != -2) {
		free(parent);
		return -ENOENT;
	}

	path = strdup(pth);
	dir_name = path + split_parent(path) + 1;
	int node_index = get_new_node_index();
	if (node_index == -1) {
		free(path);
		free(parent);
		return -ENOSPC;
	}

	size_t name_len = strlen(dir_name) + 1;

	memcpy(nodes->nodes[node_index].name,
		dir_name,
		name_len > MAX_NAME_LEN
			? MAX_NAME_LEN
			: name_len);
	nodes->nodes[node_index].size = -2;
	memset(nodes->nodes[node_index].sectors, 0, MAX_SECTORS_PER_FILE);

	int sector = fs_get_free_sector();
	if (sector == -1) {
		free(path);
		free(parent);
		return -ENOSPC;
	}
	map_set_bit(sector);
	nodes->nodes[node_index].sectors[0] = sector;
	device_clear_sector(sector, 0);

	if ( -1 == write_node_info_to_parent_sector
			(dir_name, parent->sectors[0], node_index)) {
		free(path);
		free(parent);
		return -EFAULT;
	}

	free(parent);
	free(path);

	device_flush();

	return 0;
}

int fs_unlink(const char *path)
{
	printf("\n%s(%s)\n", __func__, path);
	struct node *node = find_node_by_name(path);
	struct node *parent_node = find_parent_node(path);
	int i;

	for (i = 0; node->sectors[i] != 0; i++)
		map_clear_bit(node->sectors[i]);

	struct file_info *parent_fi = get_file_info_by_node(parent_node);
	unsigned char *par_data = parent_fi->f_data;

	for (i = 0; i < SECTOR_SIZE; i++)
		if (!strncmp((char *)par_data + i, node->name, MAX_NAME_LEN))
			break;
	// TODO: not found

	for ( i += 8; i < SECTOR_SIZE; i++)
		par_data[ i - 8 ] = par_data[i];

	delete_node_by_name(path);

	device_write_sector(par_data, parent_node->sectors[0]);
	device_flush();
	free(node);
	free(parent_fi);
	free(parent_node);

	return 0;
}

int fs_rmdir(const char *path)
{
	printf("\n%s(%s)\n", __func__, path);

	struct node* node = find_node_by_name(path);
	if (node->size == -1) {
		free(node);
		return -ENOENT;
	}

	struct file_info* dir_info = get_file_info_by_node(node);
	if ( dir_info->f_data[0] != 0) {
		free(node);
		free(dir_info);
		return -ENOTEMPTY;
	}
	free(dir_info);

	int i;
	for (i = 0; node->sectors[i] != 0; i++)
		map_clear_bit(node->sectors[i]);

	struct node* parent_node = find_parent_node(path);
	struct file_info* parent_fi = get_file_info_by_node(parent_node);
	unsigned char* par_data = parent_fi->f_data;

	for (i = 0; i < SECTOR_SIZE; i++)
		if (strncmp( (char*)(par_data + i), node->name, MAX_NAME_LEN) == 0)
			break;

	for ( i += 8; i < SECTOR_SIZE; i++)
		par_data[ i - 8 ] = par_data[i];

	delete_node_by_name(path);

	device_write_sector(par_data, parent_node->sectors[0]);
	device_flush();
	free(node);
	free(parent_node);
	free(parent_fi);
	return 0;
}

int fs_utimens(const char *path, const struct timespec tv[2])
{
	/* Yes, it should be like this */
	return 0;
}

int fs_truncate(const char *pth, off_t new_size)
{
	printf("\n%s(%s, new_size=%ld)\n", __func__, pth, new_size);

	if (new_size > MAX_FILE_SIZE)
		return -EFBIG;
	char *path = strdup(pth);
	struct node* node = find_node_by_name(path);
	if (node->size < 0) {
		free(node);
		free(path);
		return node->size == -2 ? -EISDIR : -ENOENT;
	}
	struct file_info *fi = get_file_info_by_node(node);

	if (new_size > node->size) {
		int res = fs_grow_file(fi, new_size);
		free(fi);
		free(node);
		free(path);
		return res;
	}
	int sectors_count = fs_sectors_count(new_size);
	int i;
	for ( i = sectors_count; node->sectors[i] != 0; i++) {
		map_clear_bit(node->sectors[i]);
		node->sectors[i] = 0;
	}
	memset(fi->f_data + new_size, 0, node->size - new_size);
	node->size = new_size;

	struct node *parent = find_parent_node(path);
	struct file_info* parent_fi = get_file_info_by_node(parent);
	free(parent);

	for (int i = 0; i < SECTOR_SIZE; i += DENTRY_SIZE)
		if (!strncmp((char *) parent_fi->f_data + i, node->name, MAX_NAME_LEN))
			break;
	// TODO: not found

	int node_number = parent_fi->f_data[i + 7];
	free(parent_fi);

	nodes->nodes[node_number].size = new_size;
	memcpy(nodes->nodes[node_number].sectors, node->sectors, MAX_SECTORS_PER_FILE);

	free(path);
	free(node);
	return 0;
}

int fs_open(const char *path, struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s)\n", __func__, path);
	struct node *node = find_node_by_name(path);
	struct file_info* fi = get_file_info_by_node(node);
	fileInfo->fh = (uint64_t)fi;

	return 0;
}

int fs_release(const char *path, struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s)\n", __func__, path);
	struct file_info *fi = (struct file_info *)fileInfo->fh;
	free(fi->f_node);
	free(fi);
	fileInfo->fh = 0;
	return 0;
}

int fs_read(const char *path, char *buf, size_t size, off_t offset,
		struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s, size=%lu, off=%ld)\n", __func__, path, size, offset);
	struct file_info* fi = (struct file_info*) fileInfo->fh;
	int file_size = fi->f_node->size;

	if (offset >= file_size)
		return 0;

	int bytes_to_read = size;
	if (offset + size > file_size)
		bytes_to_read = file_size - offset;

	memcpy(buf, fi->f_data + offset, bytes_to_read);

	return bytes_to_read;
}

int fs_write(const char *path, const char *buf, size_t size,
		off_t offset, struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s, size=%lu, off=%ld)\n", __func__, path, size, offset);
	struct file_info* fi = (struct file_info*) fileInfo->fh;
	fi->need_update = 1;

	if (offset + size > fi->f_node->size) {
		if (offset + size > MAX_FILE_SIZE)
			return -EFBIG;

		int new_size = (offset + size);
		int ret = fs_grow_file(fi, new_size);
		if (ret < 0)
			return ret;
		fi->f_node->size = new_size;
	}

	memcpy(fi->f_data + offset, buf, size);

	return size;
}

int fs_statfs(const char *path, struct statvfs *statInfo)
{
	printf("\n%s(%s)\n", __func__, path);
	int free_blocks = fs_count_free_blocks();

	statInfo->f_bsize = SECTOR_SIZE;
	statInfo->f_frsize = SECTOR_SIZE;
	statInfo->f_blocks = SECTOR_SIZE;
	statInfo->f_bfree = free_blocks;
	statInfo->f_bavail = free_blocks;
	statInfo->f_fsid = 0;
	statInfo->f_flag = 0;
	statInfo->f_namemax = MAX_NAME_LEN;
	return 0;
}

int fs_flush(const char *path, struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s)\n", __func__, path);
	struct file_info* fi = (struct file_info*) fileInfo->fh;
	if (fi->need_update == 0)
		return 0;
	unsigned char* ptr_data = fi->f_data;
	for (int i = 0; fi->f_node->sectors[i] != 0; ++i) {
		device_write_sector(ptr_data, fi->f_node->sectors[i]);
		ptr_data += SECTOR_SIZE;
	}

	device_flush();
	return 0;
}

int fs_opendir(const char *path, struct fuse_file_info *fileInfo) {
	printf("\n%s(%s)\n", __func__, path);
	struct node* n = find_node_by_name(path);
	int ret = n->size == -2 ? 0 : -ENOENT;
	free(n);
	return ret;
}

int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
		off_t offset, struct fuse_file_info *fileInfo)
{
	printf("\n%s(%s)\n", __func__, path);
	struct node* n = find_node_by_name(path);
	if (n->size == -1) {
		puts("\tError while readdir");
		free(n);
		return -ENOENT;
	}
	if (n->size != -2) {
		free(n);
		return -ENOENT;
	}
	filler(buf, ".", NULL, 0);
	filler(buf, "..", NULL, 0);
	struct file_info* fi = get_file_info_by_node(n);
	int name_index = 0;
	char name[MAX_NAME_LEN+1] = {0};
	while (fi->f_data[name_index] != 0 && name_index < SECTOR_SIZE) {
		memcpy (name, &(fi->f_data[name_index]), MAX_NAME_LEN);
		filler(buf, name, NULL, 0);
		name_index += DENTRY_SIZE;
	}
	free(fi);
	free(n);
	return 0;
}
